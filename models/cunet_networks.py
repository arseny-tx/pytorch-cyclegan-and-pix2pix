


import torch
import torch.nn as nn
from torch.nn import init

import functools as ft
import functools

import os, sys
from torch.optim import lr_scheduler

from torch.nn.functional import interpolate
import torch.nn.functional as F

from .networks import ResnetBlock
from .c_networks import PcResnetBlock, PatchedAdaIn, PcSequential

from functools import partial


class CondUnetModel(nn.Module):
    def __init__(self, input_nc, ngf=64, norm_layer=nn.BatchNorm2d, n_downsample=2, mults=[], use_dropout=False, 
            n_blocks=1, padding_type='reflect', use_bias=True):

        super(CondUnetModel, self).__init__() 

        if type(norm_layer) == functools.partial:
            use_bias = norm_layer.func == nn.InstanceNorm2d
        else:
            use_bias = norm_layer == nn.InstanceNorm2d

        def block(input_nc, inner_nc, relu=True):
            downconv = nn.Conv2d(input_nc, inner_nc, kernel_size=4,
                             stride=2, padding=1, bias=use_bias)
            downrelu = nn.LeakyReLU(0.2, True)

            downnorm = norm_layer(inner_nc)

            return nn.Sequential(
                *([downrelu] if relu else []), 
                downconv, downnorm
            )

        self.head = block(input_nc, ngf, relu=False)

        blocks = [
            block(ngf*n, ngf*m)
                for n, m, in zip(mults[:-1], mults[1:])
        ]

        self.blocks = nn.ModuleList(blocks)



    def forward(self, x):
        x = self.head(x)
        cond = []

        for block in self.blocks:
            x = block(x)
            cond.append(x)

        return cond



class CondUnetGenerator(nn.Module):
    """Create a Unet-based generator"""

    def __init__(self, input_nc, output_nc, num_downs, ngf=64, norm_layer=nn.BatchNorm2d, use_dropout=False):
        """Construct a Unet generator
        Parameters:
            input_nc (int)  -- the number of channels in input images
            output_nc (int) -- the number of channels in output images
            num_downs (int) -- the number of downsamplings in UNet. For example, # if |num_downs| == 7,
                                image of size 128x128 will become of size 1x1 # at the bottleneck
            ngf (int)       -- the number of filters in the last conv layer
            norm_layer      -- normalization layer

        We construct the U-Net from the innermost layer to the outermost layer.
        It is a recursive process.
        """
        mk_cnorm = lambda c_ix: partial(IxedPatchedAdaIn, cond_index=c_ix)
        super(CondUnetGenerator, self).__init__()
        # construct unet structure
        unet_block = CondUnetSkipConnectionBlock(ngf * 8, ngf * 8, input_nc=None, submodule=None, norm_layer=norm_layer, innermost=True)  # add the innermost layer
        for i in range(num_downs - 5):          # add intermediate layers with ngf * 8 filters
            unet_block = CondUnetSkipConnectionBlock(ngf * 8, ngf * 8, input_nc=None, submodule=unet_block, norm_layer=norm_layer, use_dropout=use_dropout, cnorm_layer=mk_cnorm(3))
        # gradually reduce the number of filters from ngf * 8 to ngf
        unet_block = CondUnetSkipConnectionBlock(ngf * 4, ngf * 8, input_nc=None, submodule=unet_block, norm_layer=norm_layer, cnorm_layer=mk_cnorm(2))
        unet_block = CondUnetSkipConnectionBlock(ngf * 2, ngf * 4, input_nc=None, submodule=unet_block, norm_layer=norm_layer, cnorm_layer=mk_cnorm(1))
        unet_block = CondUnetSkipConnectionBlock(ngf, ngf * 2, input_nc=None, submodule=unet_block, norm_layer=norm_layer, cnorm_layer=mk_cnorm(0))

        self.model = CondUnetSkipConnectionBlock(output_nc, ngf, input_nc=input_nc, submodule=unet_block, outermost=True, norm_layer=norm_layer)  # add the outermost layer


        self.block_kw = dict(use_dropout=False, padding_type='reflect',     
                    use_bias=True, norm_layer=nn.BatchNorm2d)


        self.cond_model = CondUnetModel(input_nc=input_nc, ngf=ngf, mults=[1, 2, 4, 8, 8], **self.block_kw)

    def forward(self, input, cond_input, patches):
        """Standard forward"""

        cs = self.cond_model(cond_input)

        return self.model(input, cs, patches)

class IxedPatchedAdaIn(PatchedAdaIn):
    def __init__(self, n, cond_index):
        super().__init__(n)
        self.cond_index = cond_index

    def forward(self, input, cs, patches):
        return super().forward(input, cs[self.cond_index], patches)

class CondUnetSkipConnectionBlock(nn.Module):
    """Defines the Unet submodule with skip connection.
        X -------------------identity----------------------
        |-- downsampling -- |submodule| -- upsampling --|
    """

    def __init__(self, outer_nc, inner_nc, input_nc=None,
                 submodule=None, outermost=False, innermost=False, 
                 norm_layer=nn.BatchNorm2d, use_dropout=False,
                 cnorm_layer=PatchedAdaIn
                 ):
        """Construct a Unet submodule with skip connections.

        Parameters:
            outer_nc (int) -- the number of filters in the outer conv layer
            inner_nc (int) -- the number of filters in the inner conv layer
            input_nc (int) -- the number of channels in input images/features
            submodule (UnetSkipConnectionBlock) -- previously defined submodules
            outermost (bool)    -- if this module is the outermost module
            innermost (bool)    -- if this module is the innermost module
            norm_layer          -- normalization layer
            use_dropout (bool)  -- if use dropout layers.
        """
        super(CondUnetSkipConnectionBlock, self).__init__()
        self.outermost = outermost
        if type(norm_layer) == functools.partial:
            use_bias = norm_layer.func == nn.InstanceNorm2d
        else:
            use_bias = norm_layer == nn.InstanceNorm2d
        if input_nc is None:
            input_nc = outer_nc

        self.layer_name = f"{outer_nc}-{inner_nc}-{input_nc}"

        downconv = nn.Conv2d(input_nc, inner_nc, kernel_size=4,
                             stride=2, padding=1, bias=use_bias)
        downrelu = nn.LeakyReLU(0.2, True)

        # downnorm = norm_layer(inner_nc)
        downnorm = cnorm_layer(inner_nc)

        uprelu = nn.ReLU(True)
        upnorm = norm_layer(outer_nc)

        if outermost:
            upconv = nn.ConvTranspose2d(inner_nc * 2, outer_nc,
                                        kernel_size=4, stride=2,
                                        padding=1)
            down = [downconv]
            up = [uprelu, upconv, nn.Tanh()]
            model = down + [submodule] + up
        elif innermost:
            upconv = nn.ConvTranspose2d(inner_nc, outer_nc,
                                        kernel_size=4, stride=2,
                                        padding=1, bias=use_bias)
            down = [downrelu, downconv]
            up = [uprelu, upconv, upnorm]
            model = down + up
        else:
            upconv = nn.ConvTranspose2d(inner_nc * 2, outer_nc,
                                        kernel_size=4, stride=2,
                                        padding=1, bias=use_bias)
            down = [downrelu, downconv, downnorm]
            up = [uprelu, upconv, upnorm]

            if use_dropout:
                model = down + [submodule] + up + [nn.Dropout(0.5)]
            else:
                model = down + [submodule] + up

        self.model = PcSequential(
                (PatchedAdaIn, 
                    IxedPatchedAdaIn, 
                    CondUnetSkipConnectionBlock), *model)

    def forward(self, input, cond_input, patches):
        try:
            if self.outermost:
                return self.model(input, cond_input, patches)
            else:   # add skip connections
                return torch.cat([
                    input, self.model(input, cond_input, patches)
                ], 1)
        except: 
            print(f"Failure in {self.layer_name}")
            print([c.shape for c in cond_input])
            raise



