import torch
import torch.nn as nn
from torch.nn import init

import functools as ft
import functools

import os, sys
from torch.optim import lr_scheduler

from torch.nn.functional import interpolate
import torch.nn.functional as F

from .networks import ResnetBlock
from .c_networks import PcResnetBlock, PatchedAdaIn, PcSequential

from functools import partial

class CondModel(nn.Module):
    def __init__(self, input_nc, ngf=64, norm_layer=nn.BatchNorm2d, n_downsample=2, mults=[], use_dropout=False, 
            n_blocks=1, padding_type='reflect', use_bias=True):

        super(CondModel, self).__init__() 

        self.head = nn.Sequential(nn.ReflectionPad2d(3),
                 nn.Conv2d(input_nc, ngf, kernel_size=7, padding=0, bias=use_bias),
                 norm_layer(ngf),
                 nn.ReLU(True))

        if not mults:
            mults = list(map(lambda x: 2**x, range(n_downsample+1)))

        down = [
            nn.Sequential(
                nn.Conv2d(ngf * mult, ngf * mult * 2, kernel_size=3, stride=2, padding=1, bias=use_bias),
                norm_layer(ngf * mult * 2),
                nn.ReLU(True)
            ) for mult in mults[:-1]
        ]
        self.down = nn.ModuleList(down)

        if n_blocks == 1:
            blocks = [
                ResnetBlock(ngf * mult * 2 , 
                    padding_type=padding_type, norm_layer=norm_layer, 
                    use_dropout=use_dropout, use_bias=use_bias
                ) for mult in mults[:-1]
            ]

            self.blocks = nn.ModuleList(blocks)
        else:
            self.blocks = [ 
                (lambda x:x) 
                    for _ in range(len(mults[:-1]))
            ]


    def forward(self, x):
        x = self.head(x)
        cond = []

        for down, block in zip(self.down, self.blocks):
            x = down(x)
            cond.append(block(x))

        return cond


class SkipConditionedResnetGenerator(nn.Module):

    def _build_model(self, input_nc, output_nc, ngf=64, n_downsample=2, 
            norm_layer=nn.BatchNorm2d, cnorm_layer=PatchedAdaIn,   
            n_blocks=6):

        _Sequential = ft.partial(PcSequential, (PcResnetBlock, PatchedAdaIn)) 
        use_bias = self.block_kw['use_bias']

        self.head = nn.Sequential(
            nn.ReflectionPad2d(3),
            nn.Conv2d(input_nc, ngf, kernel_size=7, padding=0, bias=use_bias),
            norm_layer(ngf),
            nn.ReLU(True)
        )

        mults = list(map(lambda x: 2**x, range(n_downsample+1)))

        down = [
            _Sequential(
                nn.Conv2d(ngf * mult, ngf * mult * 2, kernel_size=3, stride=2, padding=1, bias=use_bias),
                cnorm_layer(ngf * mult * 2), # if 0 in cond_skips else norm_layer, 
                nn.ReLU(True)
            ) for mult in mults[:-1]
        ]

        self.down = nn.ModuleList(down)

        mult = mults[-1]
        blocks = []
        for i in range(n_blocks):
            blocks += [
                PcResnetBlock(ngf * mult, **self.block_kw)
            ]
        self.blocks = _Sequential(*blocks)

        
        up = [
            nn.Sequential(nn.ConvTranspose2d(ngf * mult * 2, int(ngf * mult / 2),
                                     kernel_size=3, stride=2,
                                     padding=1, output_padding=1,
                                     bias=use_bias),
                  norm_layer(int(ngf * mult / 2)),
                  nn.ReLU(True)
            ) for mult in reversed(mults[1:])
        ]

        self.up = nn.ModuleList(up)

        self.tail = nn.Sequential(
            nn.ReflectionPad2d(3),
            nn.Conv2d(ngf, output_nc, kernel_size=7, padding=0),
            nn.Tanh()
        )

        # self.alphas = nn.Parameter(torch.zeros(2))


    def forward(self, input, cond_input, patches):

        cs = self.cmodel(cond_input)

        x = self.head(input)

        ds = []

        for down, c in zip(self.down, cs):
            x = down(x, c, patches)
            ds.append(x)

        x = self.blocks(x, cs[-1], patches)

        for up, d in zip(self.up, reversed(ds)):
            x = torch.cat([x, d], axis=1)
            x = up(x)

        x = self.tail(x)

        return x


    def __init__(self, input_nc, output_nc, ngf=64, n_blocks=6, n_downsample=2, norm_layer=nn.BatchNorm2d, 
                    use_dropout=False, padding_type='reflect', 
                    use_bias=True):

        """Construct a Resnet-based generator

        Parameters:
            input_nc (int)      -- the number of channels in input images
            output_nc (int)     -- the number of channels in output images
            ngf (int)           -- the number of filters in the last conv layer
            norm_layer          -- normalization layer
            use_dropout (bool)  -- if use dropout layers
            n_blocks (int)      -- the number of ResNet blocks
            padding_type (str)  -- the name of padding layer in conv layers: reflect | replicate | zero
        """
        assert(n_blocks >= 0)
        super(SkipConditionedResnetGenerator, self).__init__()
        if type(norm_layer) == functools.partial:
            use_bias = norm_layer.func == nn.InstanceNorm2d
        else:
            use_bias = norm_layer == nn.InstanceNorm2d



        self.block_kw = dict(padding_type=padding_type, 
            norm_layer=norm_layer, use_dropout=use_dropout, 
            use_bias=use_bias)

        self._build_model(
            input_nc=input_nc, output_nc=output_nc, n_downsample=n_downsample, 
            norm_layer=norm_layer, n_blocks=n_blocks, 
            ngf=ngf)



        self.cmodel = CondModel(input_nc=input_nc, ngf=ngf, n_downsample=n_downsample, **self.block_kw)

        # print(self)

