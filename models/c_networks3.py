import torch
import torch.nn as nn
from torch.nn import init
import functools

import os, sys

import warnings
from torch.nn.functional import interpolate



import torch.nn.functional as F

class PatchedAdaIn(nn.Module):

    def __init__(self, nch):
        super().__init__()

    def forward(self, x, c, b_patches):
        # x ~ b x c x h x w
        # c ~ b x c x h x w
        # b_patches ~ b x [ n_pathces x h x w ]
        assert x.shape == c.shape, f'x and c shapes don\'t  coincide ! {x.shape} {c.shape}'
        y = F.instance_norm(x)
        eps = 1e-5
        for batch_ix, patches in enumerate(b_patches):

            # n_patches x 1 x W x H
            patches = interpolate(patches.unsqueeze(1), size=c.size()[2:]).detach() 
            
            t = c[batch_ix].unsqueeze(0) * patches # n_patches x n_channels x W x H

            # n_patches x n_channels x 1 x 1
            std  = (t.var([2, 3], keepdim=True) + eps).sqrt()
            mean = t.mean([2, 3], keepdim=True) 
            norm = patches.sum([2, 3], keepdim=True)

            # n_patches x n_channels x W x H
            mean = (patches * mean / ( norm + eps )).sum(0)
            std  = (patches * std  / ( norm + eps )).sum(0)

            y[batch_ix, ...] *= std
            y[batch_ix, ...] += mean 
        # print('--------')

        return y

class PcSequential(nn.Sequential):
    def forward(self, input, c, patches):
        for module in self._modules.values():
            if isinstance(module, PatchedAdaIn):
                input = module(input, c, patches)
            else:
                input = module(input)
        return input



class PcResnetBlock(nn.Module):
    """Define a Resnet block"""

    def __init__(self, dim, padding_type, norm_layer, use_dropout, use_bias):
        """Initialize the Resnet block

        A resnet block is a conv block with skip connections
        We construct a conv block with build_conv_block function,
        and implement skip connections in <forward> function.
        Original Resnet paper: https://arxiv.org/pdf/1512.03385.pdf
        """
        super(PcResnetBlock, self).__init__()
        self.conv_block = self.build_conv_block(dim, padding_type, norm_layer, use_dropout, use_bias)

    def build_conv_block(self, dim, padding_type, norm_layer, use_dropout, use_bias):
        """Construct a convolutional block.

        Parameters:
            dim (int)           -- the number of channels in the conv layer.
            padding_type (str)  -- the name of padding layer: reflect | replicate | zero
            norm_layer          -- normalization layer
            use_dropout (bool)  -- if use dropout layers.
            use_bias (bool)     -- if the conv layer uses bias or not

        Returns a conv block (with a conv layer, a normalization layer, and a non-linearity layer (ReLU))
        """
        conv_block = []
        p = 0
        if padding_type == 'reflect':
            conv_block += [nn.ReflectionPad2d(1)]
        elif padding_type == 'replicate':
            conv_block += [nn.ReplicationPad2d(1)]
        elif padding_type == 'zero':
            p = 1
        else:
            raise NotImplementedError('padding [%s] is not implemented' % padding_type)

        conv_block += [
            nn.Conv2d(dim, dim, kernel_size=3, padding=p, bias=use_bias), 
            norm_layer(dim),
            nn.ReLU(True) # if act == 'relu' else nn.LeackyReLU(0.2, True)
        ]
        if use_dropout:
            conv_block += [nn.Dropout(0.5)]

        p = 0
        if padding_type == 'reflect':
            conv_block += [nn.ReflectionPad2d(1)]
        elif padding_type == 'replicate':
            conv_block += [nn.ReplicationPad2d(1)]
        elif padding_type == 'zero':
            p = 1
        else:
            raise NotImplementedError('padding [%s] is not implemented' % padding_type)
        conv_block += [nn.Conv2d(dim, dim, kernel_size=3, padding=p, bias=use_bias), PatchedAdaIn(dim)]

        return PcSequential(*conv_block)

    def forward(self, x, c, patches):
        """Forward function (with skip connections)"""
        out = x + self.conv_block(x, c, patches) 
        return out

from .networks import ResnetBlock


        
class ConditionedResnetGeneratorOld(nn.Module):

    def _build_model(self, input_nc, output_nc, ngf=64, 
            norm_layer=nn.BatchNorm2d, cnorm_layer=PatchedAdaIn,  use_dropout=False, 
            n_blocks=6, padding_type='reflect', use_bias=True, 
            c_norm_pos='mid'):

        model = [
            nn.ReflectionPad2d(3),
            nn.Conv2d(input_nc, ngf, kernel_size=7, padding=0, bias=use_bias),
            norm_layer(ngf),
            nn.ReLU(True)
        ]

        n_downsampling = 2
        for i in range(n_downsampling):  # add downsampling layers
            mult = 2 ** i
            model += [nn.Conv2d(ngf * mult, ngf * mult * 2, kernel_size=3, stride=2, padding=1, bias=use_bias),
                      norm_layer(ngf * mult * 2),
                      nn.ReLU(True)]

        mult = 2 ** n_downsampling
        for i in range(n_blocks):       # add ResNet blocks
            # _norm_layer = cnorm_layer 
            Block = PcResnetBlock if 'mid' in c_norm_pos else ResnetBlock
            model += [
                Block(ngf * mult, padding_type=padding_type, 
                                    norm_layer=norm_layer, use_dropout=use_dropout, 
                                    use_bias=use_bias)
            ]

        for i in range(n_downsampling):  # add upsampling layers
            mult = 2 ** (n_downsampling - i)
            model += [nn.ConvTranspose2d(ngf * mult, int(ngf * mult / 2),
                                         kernel_size=3, stride=2,
                                         padding=1, output_padding=1,
                                         bias=use_bias),
                      norm_layer(int(ngf * mult / 2)),
                      nn.ReLU(True)]
        model += [nn.ReflectionPad2d(3)]
        model += [nn.Conv2d(ngf, output_nc, kernel_size=7, padding=0)]
        model += [nn.Tanh()]

        return nn.ModuleList(model)

    def _build_cmodel(self, input_nc, output_nc, ngf=64, 
            norm_layer=nn.BatchNorm2d, use_dropout=False, 
            n_blocks=6, padding_type='reflect', use_bias=True):
        
        model = [nn.ReflectionPad2d(3),
                 nn.Conv2d(input_nc, ngf, kernel_size=7, padding=0, bias=use_bias),
                 norm_layer(ngf),
                 nn.ReLU(True)]

        n_downsampling = 2
        for i in range(n_downsampling):  # add downsampling layers
            mult = 2 ** i
            model += [nn.Conv2d(ngf * mult, ngf * mult * 2, kernel_size=3, stride=2, padding=1, bias=use_bias),
                            norm_layer(ngf * mult * 2),
                            nn.ReLU(True)]

        mult = 2 ** n_downsampling
        for i in range(n_blocks):       # add ResNet blocks
            model += [ResnetBlock(ngf * mult, padding_type=padding_type, norm_layer=norm_layer, use_dropout=use_dropout, use_bias=use_bias)]

        return nn.Sequential(*model)


    def __init__(self, input_nc, output_nc, ngf=64, norm_layer=nn.BatchNorm2d, use_dropout=False, n_blocks=6, n_style_blocks=1, num_cs=None, padding_type='reflect'):
        """Construct a Resnet-based generator

        Parameters:
            input_nc (int)      -- the number of channels in input images
            output_nc (int)     -- the number of channels in output images
            ngf (int)           -- the number of filters in the last conv layer
            norm_layer          -- normalization layer
            use_dropout (bool)  -- if use dropout layers
            n_blocks (int)      -- the number of ResNet blocks
            padding_type (str)  -- the name of padding layer in conv layers: reflect | replicate | zero
        """
        assert(n_blocks >= 0)
        super(ConditionedResnetGeneratorOld, self).__init__()
        if type(norm_layer) == functools.partial:
            use_bias = norm_layer.func == nn.InstanceNorm2d
        else:
            use_bias = norm_layer == nn.InstanceNorm2d

        args = dict(input_nc=input_nc, output_nc=output_nc, norm_layer=norm_layer, 
            use_dropout=use_dropout, padding_type=padding_type, 
            use_bias=use_bias)


        self.model = self._build_model(n_blocks=n_blocks, **args)
        assert n_style_blocks == 1
        self.cmodel = self._build_cmodel(n_blocks=n_style_blocks, **args)

    def forward(self, input, c, patches):
        """Standard forward"""

        c = self.cmodel(c)

        for m in self.model:
            if isinstance(m, PcResnetBlock):
                input = m(input, c, patches)
            else:
                input = m(input)


        return input # self.model(input)
