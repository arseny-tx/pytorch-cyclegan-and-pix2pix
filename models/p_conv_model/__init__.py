import torch

from ..base_model import BaseModel


from inspect import signature
import warnings

from .loss import InpaintingLoss
from .net import PConvUNet
from .net import VGG16FeatureExtractor


class PConvModel(BaseModel):
    """ This class implements the pix2pix model, for learning a mapping from input images to output images given paired data.

    The model training requires '--dataset_mode aligned' dataset.
    By default, it uses a '--netG unet256' U-Net generator,
    a '--netD basic' discriminator (PatchGAN),
    and a '--gan_mode' vanilla GAN loss (the cross-entropy objective used in the orignal GAN paper).

    pix2pix paper: https://arxiv.org/pdf/1611.07004.pdf
    """
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        """Add new dataset-specific options, and rewrite default values for existing options.

        Parameters:
            parser          -- original option parser
            is_train (bool) -- whether training phase or test phase. You can use this flag to add training-specific or test-specific options.

        Returns:
            the modified parser.

        For pix2pix, we do not use image buffer
        The training objective is: GAN Loss + lambda_L1 * ||G(A)-B||_1
        By default, we use vanilla GAN loss, UNet with batchnorm, and aligned datasets.
        """
        # changing the default values to match the pix2pix paper (https://phillipi.github.io/pix2pix/)
        parser.set_defaults( dataset_mode='aligned', direction='AtoB')
        

        if is_train:

            parser.add_argument('--lambda_tv', type=float, default=0.1, help='Total Variation loss')
            parser.add_argument('--lambda_prc', type=float, default=0.05, help='Perceptual loss')
            parser.add_argument('--lambda_style', type=float, default=120., help='Style loss')

            parser.add_argument('--lambda_valid', type=float, default=1.0, help='Valid L1 loss')
            parser.add_argument('--lambda_hole', type=float, default=6.0, help='Hole L1 loss')
            parser.add_argument('--lrG', type=float, default=2e-4, help='generator learning rate')

            parser.add_argument('--mask_as_separate_channel', action='store_true', 
                    help='feed the mask as a separate cannel')

            parser.add_argument('--net_cfg', type=str, default='2skips', 
                    help='choose net configuration')


        return parser

    def __init__(self, opt):
        """Initialize the pix2pix class.

        Parameters:
            opt (Option class)-- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseModel.__init__(self, opt)
        
        assert opt.direction == 'AtoB'

        self.crit_losses = ['tv', 'prc', 'style', 'valid', 'hole']
        self.loss_names = [ f'L_{l}' for l in self.crit_losses ]



        self.visual_names = [
            'fake_B',  'real_B', 
            'mask',  'style_img', # 'tv_mask'
        ]

        self.visual_names.insert(0, 'real_A')
        

        self.model_names = ['G']

        input_nc = opt.input_nc + int(opt.mask_as_separate_channel)


        self.netG = PConvUNet(input_channels=input_nc, output_channels=opt.output_nc).to(self.device)

        if self.isTrain:

            self.criterion = InpaintingLoss(VGG16FeatureExtractor()).to(self.device)

            # define loss functions
            self.optimizer_G = torch.optim.Adam(self.netG.parameters(), lr=opt.lrG, betas=(opt.beta1, 0.999))

            self.optimizers.append(self.optimizer_G)

        

    def backward_G(self):
        

        loss_dict = self.criterion(self.real_A, self.mask, self.fake_B, self.real_B)

        loss = 0.0
        for key in self.crit_losses:

            coef = getattr(self.opt, f'lambda_{key}')
            value = coef * loss_dict[key]
            loss += value

            setattr(self, f'loss_L_{key}', value)

        loss.backward()


    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.

        Parameters:
            input (dict): include the data itself and its metadata information.

        The option 'direction' can be used to swap images in domain A and domain B.
        """
        # AtoB = self.opt.direction == 'AtoB'

        self.real_A = input['A'].to(self.device)
        self.real_B = input['B'].to(self.device)

        self.mask = input['mask'].to(self.device)

        # if not self.opt.mask_as_separate_channel:            
            

        # if 'patch_edges' in input:
        #    self.patch_edges = input['patch_edges'].to(self.device)
        #else:
        #    warnings.warn('no patch_edges supplied')

        # if 'tv_mask' in input:
        #     self.tv_mask = input['tv_mask'].to(self.device)
            

        if 'style_img' in input:
            self.style_img = input['style_img'].to(self.device)
        else:
            self.style_img = self.real_B


        self.patches = [ m.to(self.device) for m in input['patches']]
        # self.computed_image = 
        # print('patches', len(input['patches']), [p.shape for p in input['patches']])
        self.image_paths = input['A_paths']



    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""

        

        if self.opt.mask_as_separate_channel:
            
            real_A = torch.cat([ 
                ( self.real_A * (1 - self.mask) ).sum(1).unsqueeze(1),
                self.real_A * self.mask + (1-self.mask)
            ], dim=1)
            mask = self.mask.repeat(1, 4, 1, 1)

        else:

            real_A = self.real_A
            mask = 1 - (1 - self.mask) * self.real_A
             
        self.fake_B, _ = self.netG(real_A, mask, self.style_img, self.patches)
        # if self.opt.inpaint_losses:
        self.comp_B = self.fake_B * (1-self.mask) + self.real_B * self.mask

    def optimize_parameters(self):
        self.forward()                   # compute fake images: G(A)

        self.optimizer_G.zero_grad()        # set G's gradients to zero
        self.backward_G()                   # calculate graidents for G
        self.optimizer_G.step()             # udpate G's weights