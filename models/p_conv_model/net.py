import math

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models

from ..c_networks import PatchedAdaIn


def weights_init(init_type='gaussian'):
    def init_fun(m):
        classname = m.__class__.__name__
        if (classname.find('Conv') == 0 or classname.find(
                'Linear') == 0) and hasattr(m, 'weight'):
            if init_type == 'gaussian':
                nn.init.normal_(m.weight, 0.0, 0.02)
            elif init_type == 'xavier':
                nn.init.xavier_normal_(m.weight, gain=math.sqrt(2))
            elif init_type == 'kaiming':
                nn.init.kaiming_normal_(m.weight, a=0, mode='fan_in')
            elif init_type == 'orthogonal':
                nn.init.orthogonal_(m.weight, gain=math.sqrt(2))
            elif init_type == 'default':
                pass
            else:
                assert 0, "Unsupported initialization: {}".format(init_type)
            if hasattr(m, 'bias') and m.bias is not None:
                nn.init.constant_(m.bias, 0.0)

    return init_fun


class VGG16FeatureExtractor(nn.Module):
    def __init__(self):
        super().__init__()
        vgg16 = models.vgg16(pretrained=True)
        self.enc_1 = nn.Sequential(*vgg16.features[:5])
        self.enc_2 = nn.Sequential(*vgg16.features[5:10])
        self.enc_3 = nn.Sequential(*vgg16.features[10:17])

        # fix the encoder
        for i in range(3):
            for param in getattr(self, 'enc_{:d}'.format(i + 1)).parameters():
                param.requires_grad = False

    def forward(self, image):
        results = [image]
        for i in range(3):
            func = getattr(self, 'enc_{:d}'.format(i + 1))
            results.append(func(results[-1]))
        return results[1:]


class RegularConv(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True):
        super().__init__()
        self.input_conv = nn.Conv2d(in_channels, out_channels, kernel_size,
                                    stride, padding, dilation, groups, bias)

    def forward(self, input):    
        output = self.input_conv(input)
        return  output

class PartialConv(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True, 
                 unmasked_in_channels=0, unmasked_out_channels=0):
        super().__init__()
        self.input_conv = nn.Conv2d(

            in_channels - unmasked_in_channels, 
            out_channels - unmasked_out_channels, 

            kernel_size, stride, padding, dilation, groups, bias)

        self.mask_conv = nn.Conv2d(

            in_channels - unmasked_in_channels, 
            out_channels - unmasked_out_channels, 

            kernel_size, stride, padding, dilation, groups, False)
        self.input_conv.apply(weights_init('kaiming'))

        torch.nn.init.constant_(self.mask_conv.weight, 1.0)

        # mask is not updated
        for param in self.mask_conv.parameters():
            param.requires_grad = False

        if unmasked_in_channels > 0:
            assert unmasked_out_channels > 0
            self.unmasked_conv = nn.Conv2d(
                
                unmasked_in_channels, 
                unmasked_out_channels, 

                kernel_size, stride, padding, dilation, groups, bias)

        self.unmasked_in_channels = unmasked_in_channels
        self.unmasked_out_channels = unmasked_out_channels


    def forward(self, input, mask):
        # http://masc.cs.gmu.edu/wiki/partialconv
        # C(X) = W^T * X + b, C(0) = b, D(M) = 1 * M + 0 = sum(M)
        # W^T* (M .* X) / sum(M) + b = [C(M .* X) – C(0)] / D(M) + C(0)

        if self.unmasked_out_channels > 0:
            unmasked_input = input[:, self.unmasked_out_channels:, ...]
            masked_input   = input[:, :self.unmasked_out_channels, ...]
        else:
            masked_input   = input

        output = self.input_conv(masked_input * mask)
        if self.input_conv.bias is not None:
            output_bias = self.input_conv.bias.view(1, -1, 1, 1).expand_as(
                output)
        else:
            output_bias = torch.zeros_like(output)

        if self.unmasked_out_channels > 0:
            unmasked_output = self.unmasked_conv(unmasked_input)

            output = torch.cat([unmasked_output, output], dim=1)

        with torch.no_grad():
            output_mask = self.mask_conv(mask)

        no_update_holes = output_mask == 0
        mask_sum = output_mask.masked_fill_(no_update_holes, 1.0)

        output_pre = (output - output_bias) / mask_sum + output_bias
        output = output_pre.masked_fill_(no_update_holes, 0.0)

        new_mask = torch.ones_like(output)
        new_mask = new_mask.masked_fill_(no_update_holes, 0.0)

        return output, new_mask


class PCBActiv(nn.Module):
    def __init__(self, in_ch, out_ch, bn=True, sample='none-3', activ='relu',
                 conv_bias=False, batchnorm_layer='regular', conv='partial',
                 unm_in_ch=0, unm_out_ch=0):

        super().__init__()

        self.conv_layer = conv
        self.batchnorm_layer = bn
        self.out_ch = out_ch

        

        if conv == 'partial':
            Conv = PartialConv  
            kw = dict(
                unmasked_in_channels=unm_in_ch, 
                unmasked_out_channels=unm_out_ch
            )
        else:
            Conv = RegularConv    
            kw = dict()
        

        if sample == 'down-5':
            self.conv = Conv(in_ch, out_ch, 5, 2, 2, bias=conv_bias, **kw)
        elif sample == 'down-7':
            self.conv = Conv(in_ch, out_ch, 7, 2, 3, bias=conv_bias, **kw)
        elif sample == 'down-3':
            self.conv = Conv(in_ch, out_ch, 3, 2, 1, bias=conv_bias, **kw)
        else:
            self.conv = Conv(in_ch, out_ch, 3, 1, 1, bias=conv_bias, **kw)


        if bn == 'patched':
            self.bn = PatchedAdaIn(out_ch)
        elif bn == 'regular':
        # else:
            self.bn = nn.BatchNorm2d(out_ch) 
        


        if activ == 'relu':
            self.activation = nn.ReLU()
        elif activ == 'leaky':
            self.activation = nn.LeakyReLU(negative_slope=0.2)

    def forward(self, input, input_mask=None, cond=None, patches=None):

        if self.conv_layer == 'partial':
            assert input_mask is not None
            h, h_mask = self.conv(input, input_mask)
        else:
            h = self.conv(input)
            h_mask = None

        if hasattr(self, 'bn'):
            if self.batchnorm_layer == 'regular':
                h = self.bn(h)
            else:
                assert cond is not None and patches is not None
                h = self.bn(h, cond, patches)

        if hasattr(self, 'activation'):
            h = self.activation(h)
        return h, h_mask


from argparse import Namespace

class PConvUNet(nn.Module):


    def _get_cfg(self, name):

        cfgs = {}

        reg_norm_k, no_skip_k = 2, 2
        cfgs['2skips'] = {
            'enc_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'dec_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'skips'     : [None] + [0] * no_skip_k +          [1] * (self.layer_size - no_skip_k),

            'unm_chs'   : [None] + [0] * self.layer_size * 2 
        }

        reg_norm_k, no_skip_k = 2, 6
        cfgs['6skips'] = {
            'enc_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'dec_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'skips'     : [None] + [0] * no_skip_k +          [1] * (self.layer_size - no_skip_k),

            'unm_chs'   : [None] + [0] * self.layer_size * 2 
        }


        reg_norm_k, no_skip_k = 2, 6
        cfgs['reg_dec'] = {
            'enc_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'dec_norms' : [None] + ['none'] + ['regular'] * (self.layer_size),
            'skips'     : [None] + [0] * no_skip_k +          [1] * (self.layer_size - no_skip_k),

            'unm_chs'   : [None] + [0] * self.layer_size * 2 
        }

        
        reg_norm_k, no_skip_k = 2, 2
        cfgs['unmasked'] = {
            'enc_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'dec_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'skips'     : [None] + [0] * no_skip_k +          [1] * (self.layer_size - no_skip_k),

            'unm_chs'   : [None] + [1, 2, 4, 8, 16, 32, 16, 8, 4, 2, 1] + [0] * (self.layer_size*2 - 11)
        }


        reg_norm_k, no_skip_k = 2, 2
        cfgs['unmasked_fix'] = {
            'enc_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'dec_norms' : [None] + ['none'] + ['regular'] * (reg_norm_k-1) + ['cond_norm'] * (self.layer_size - reg_norm_k),
            'skips'     : [None] + [0] * no_skip_k +          [1] * (self.layer_size - no_skip_k),

            'unm_chs'   : [None] + [1, 4, 8, 32, 64, 128, 128] + [0] * self.layer_size,
             
            'assert' : lambda: self.layer_size == 7
        }
        
        return  Namespace(**cfgs[name])



    def __init__(self, layer_size=7, input_channels=3, output_channels=3, cond_channels=3, upsampling_mode='nearest', cfg_name='2skips'):
        super().__init__()
        self.freeze_enc_bn = False
        self.upsampling_mode = upsampling_mode
        self.layer_size = layer_size

    

        cfg = self._get_cfg(name=cfg_name)
        self.cfg = cfg


        self.cond_enc_1 = PCBActiv(cond_channels, 64, bn='none', sample='down-7', conv='regular')
        self.cond_enc_2 = PCBActiv(64, 128, sample='down-5', conv='regular')
        self.cond_enc_3 = PCBActiv(128, 256, sample='down-5', conv='regular')
        self.cond_enc_4 = PCBActiv(256, 512, sample='down-3', conv='regular')

        for i in range(4, self.layer_size):
            name = 'cond_enc_{:d}'.format(i + 1)
            setattr(self, name, PCBActiv(512, 512, sample='down-3', conv='regular'))


        #
        # Build the main network
        #

        assert cfg.enc_norms[1] == 'none'
        kw = dict(bn=cfg.enc_norms[1], unm_in_ch=cfg.unm_chs[1], unm_out_ch=cfg.unm_chs[2])
        self.enc_1 = PCBActiv(input_channels, 64, sample='down-7', **kw)

        kw = dict(bn=cfg.enc_norms[2], unm_in_ch=cfg.unm_chs[2], unm_out_ch=cfg.unm_chs[3])
        self.enc_2 = PCBActiv(64, 128, sample='down-5', **kw)

        kw = dict(bn=cfg.enc_norms[3], unm_in_ch=cfg.unm_chs[3], unm_out_ch=cfg.unm_chs[4])
        self.enc_3 = PCBActiv(128, 256, sample='down-5', **kw)

        kw = dict(bn=cfg.enc_norms[4], unm_in_ch=cfg.unm_chs[4], unm_out_ch=cfg.unm_chs[5])
        self.enc_4 = PCBActiv(256, 512, sample='down-3', **kw)

        for i in range(4, self.layer_size):
            name = 'enc_{:d}'.format(i + 1)
            kw = dict(bn=cfg.enc_norms[i+1], unm_in_ch=cfg.unm_chs[i+1], unm_out_ch=cfg.unm_chs[i+2])
            setattr(self, name, PCBActiv(512, 512, sample='down-3', **kw))

        chs_i = self.layer_size - 1
        for i in range(4, self.layer_size):
            name = 'dec_{:d}'.format(i + 1)
            kw = dict(bn=cfg.dec_norms[i+1], unm_in_ch=cfg.unm_chs[chs_i+1], unm_out_ch=cfg.unm_chs[chs_i+2])
            chs_i += 1
            setattr(self, name, PCBActiv(512 + 512*cfg.skips[i+1], 512, activ='leaky', **kw))

        kw = dict(bn=cfg.dec_norms[4], unm_in_ch=cfg.unm_chs[chs_i+1], unm_out_ch=cfg.unm_chs[chs_i+2])
        chs_i += 1

        self.dec_4 = PCBActiv(512 + 256*cfg.skips[4], 256, activ='leaky', **kw)

        kw = dict(bn=cfg.dec_norms[3], unm_in_ch=cfg.unm_chs[chs_i+1], unm_out_ch=cfg.unm_chs[chs_i+2])
        chs_i += 1

        self.dec_3 = PCBActiv(256 + 128*cfg.skips[3], 128, activ='leaky', **kw)

        kw = dict(bn=cfg.dec_norms[2], unm_in_ch=cfg.unm_chs[chs_i+1], unm_out_ch=cfg.unm_chs[chs_i+2])
        chs_i += 1

        self.dec_2 = PCBActiv(128 + 64*cfg.skips[2], 64, activ='leaky', **kw)

        assert cfg.enc_norms[1] == 'none'
        kw = dict(bn=cfg.dec_norms[1], unm_in_ch=cfg.unm_chs[chs_i+1], unm_out_ch=cfg.unm_chs[chs_i+2])
        chs_i += 1

        self.dec_1 = PCBActiv(64 + input_channels*cfg.skips[1], output_channels, activ=None, 
                conv_bias=True, **kw)


        # print(self)
        # self.module = self

    def forward(self, input, input_mask, cond, patches):

        # print('s', input.shape)

        c_h_dict = {}        
        c_h_dict['h_0'] = cond

        h_key_prev = 'h_0'
        for i in range(1, self.layer_size + 1):        
            l_key = 'cond_enc_{:d}'.format(i)
            h_key = 'h_{:d}'.format(i)

            c_h_dict[h_key], _ = getattr(self, l_key)(
                c_h_dict[h_key_prev], patches)
            h_key_prev = h_key

        c_h_last = h_key

        h_dict = {}  # for the output of enc_N
        h_mask_dict = {}  # for the output of enc_N

        h_dict['h_0'], h_mask_dict['h_0'] = input, input_mask
        

        h_key_prev = 'h_0'
        for i in range(1, self.layer_size + 1):
            l_key = 'enc_{:d}'.format(i)
            h_key = 'h_{:d}'.format(i)

            c_h_key = h_key if h_key in c_h_dict else c_h_last

            kw = {}  
            if self.cfg.enc_norms[i] != 'regular':
                kw = dict(cond=c_h_dict[c_h_key], patches=patches)

            h_dict[h_key], h_mask_dict[h_key] = getattr(self, l_key)(
                h_dict[h_key_prev], h_mask_dict[h_key_prev], **kw)
            h_key_prev = h_key

        h_key = 'h_{:d}'.format(self.layer_size)
        h, h_mask = h_dict[h_key], h_mask_dict[h_key]

        # concat upsampled output of h_enc_N-1 and dec_N+1, then do dec_N
        # (exception)
        #                            input         dec_2            dec_1
        #                            h_enc_7       h_enc_8          dec_8

        for i in range(self.layer_size, 0, -1):
            enc_h_key = 'h_{:d}'.format(i - 1)
            dec_l_key = 'dec_{:d}'.format(i)
            c_h_key = 'h_{:d}'.format(i - 1)

            h = F.interpolate(h, scale_factor=2, mode=self.upsampling_mode)
            h_mask = F.interpolate(
                h_mask, scale_factor=2, mode='nearest')

            if self.cfg.skips[i]:
                h = torch.cat([h, h_dict[enc_h_key]], dim=1)
                h_mask = torch.cat([h_mask, h_mask_dict[enc_h_key]], dim=1)

            kw = {}  
            if self.cfg.dec_norms[i] != 'regular':
                kw = dict(cond=c_h_dict[c_h_key], patches=patches)

            h, h_mask = getattr(self, dec_l_key)(h, h_mask, **kw)

        return h, h_mask

    def train(self, mode=True):
        """
        Override the default train() to freeze the BN parameters
        """
        super().train(mode)
        if self.freeze_enc_bn:
            for name, module in self.named_modules():
                if isinstance(module, nn.BatchNorm2d) and 'enc' in name:
                    module.eval()







if __name__ == '__main__':

    gpu = 1

    net = PConvUNet().to(gpu)


    input = torch.randn(2, 3, 128, 128)
    cond = torch.randn(2, 3, 128, 128)

    input_mask = (torch.randn(2, 3, 128, 128) > 0).float()

    patches = [
        torch.stack([
            (torch.randn(128, 128) > 0).float(), 
            (torch.randn(128, 128) > 0).float(),
            (torch.randn(128, 128) > 0).float()
        ], dim=0),

        torch.stack([
            (torch.randn(128, 128) > 0).float(),
            (torch.randn(128, 128) > 0).float()
        ], dim=0),
    ]

    cvt = lambda xs: list(map(lambda x: x.to(gpu), xs))
    input, input_mask, cond = cvt( (input, input_mask, cond) )

    patches = cvt(patches)

    h, _ = net(input, input_mask, cond, patches)

    print(torch.isnan(h).sum().item())
    print(h)