import torch
from .base_model import BaseModel
from . import networks

from .pix2pix_model import Pix2PixModel

from shared.losses import TVLoss, VGG19, VGG19_2, VGGLoss

from inspect import signature
import warnings



class CCannyModel(Pix2PixModel):
    """ This class implements the pix2pix model, for learning a mapping from input images to output images given paired data.

    The model training requires '--dataset_mode aligned' dataset.
    By default, it uses a '--netG unet256' U-Net generator,
    a '--netD basic' discriminator (PatchGAN),
    and a '--gan_mode' vanilla GAN loss (the cross-entropy objective used in the orignal GAN paper).

    pix2pix paper: https://arxiv.org/pdf/1611.07004.pdf
    """
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        """Add new dataset-specific options, and rewrite default values for existing options.

        Parameters:
            parser          -- original option parser
            is_train (bool) -- whether training phase or test phase. You can use this flag to add training-specific or test-specific options.

        Returns:
            the modified parser.

        For pix2pix, we do not use image buffer
        The training objective is: GAN Loss + lambda_L1 * ||G(A)-B||_1
        By default, we use vanilla GAN loss, UNet with batchnorm, and aligned datasets.
        """
        # changing the default values to match the pix2pix paper (https://phillipi.github.io/pix2pix/)
        parser.set_defaults(norm='batch', netG='unet_256', dataset_mode='aligned')
        parser.add_argument('--n_style_blocks', type=int, default=1)
        parser.add_argument('--num_cs', choices=['all', 'last'], default='last')
        

        if is_train:
            parser.set_defaults(pool_size=0, gan_mode='vanilla')

            
            parser.add_argument('--lrG', type=float, default=None, help='generator learning rate')
            parser.add_argument('--lrD', type=float, default=None, help='discriminator learning rate')

            parser.add_argument('--lambda_L1', type=float, default=100.0, help='weight for L1 loss')
            parser.add_argument('--smooth_labels', action='store_true')
            parser.add_argument('--noisy_labels', action='store_true')

            parser.add_argument('--no_D', action='store_true')
            parser.add_argument('--vgg2', action='store_true')

            parser.add_argument('--inpaint_losses', action='store_true')
            parser.add_argument('--lambda_tv', type=float, default=0.1, help='Total Variation loss')
            parser.add_argument('--lambda_perc', type=float, default=0.05, help='Perceptual loss')
            parser.add_argument('--lambda_style', type=float, default=120., help='Style loss')

            parser.add_argument('--spectral_norm', action='store_true')



        return parser

    def __init__(self, opt):
        """Initialize the pix2pix class.

        Parameters:
            opt (Option class)-- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseModel.__init__(self, opt)
        
        self.loss_names = ['G_L1']


        self.visual_names = [
            'real_A', 'fake_B',  'real_B', 
            'mask',  'tv_mask','style_img'
        ]

        if not self.opt.no_D:
            self.loss_names.extend([ 'D_real', 'D_fake','G_GAN',])

        if self.opt.inpaint_losses:
            self.loss_names.extend(['G_perc', 'G_style', 'G_tv'])
            self.visual_names.extend(['comp_B'])

        if self.opt.noise_style_image:
            self.visual_names.extend(['style_img'])            

        # specify the models you want to save to the disk. The training/test scripts will call <BaseModel.save_networks> and <BaseModel.load_networks>
        if self.isTrain and not self.opt.no_D:
            self.model_names = ['G', 'D']
        else:  # during test time, only load G
            self.model_names = ['G']
        # define networks (both generator and discriminator)
        self.netG = networks.define_G(opt.input_nc, opt.output_nc, opt.ngf, opt.netG, opt.norm,
                                      not opt.no_dropout, opt.init_type, opt.init_gain, self.gpu_ids, 
                                      n_style_blocks=opt.n_style_blocks, num_cs=opt.num_cs)

        self.num_args = len(signature(self.netG).parameters)
        print('Num args', self.num_args)

        if self.isTrain and not self.opt.no_D:  
            # define a discriminator; conditional GANs need to take both input and output images; Therefore, #channels for D is input_nc + output_nc
            self.netD = networks.define_D(opt.input_nc + opt.output_nc, opt.ndf, opt.netD,
                                          opt.n_layers_D, opt.norm, opt.init_type, opt.init_gain, self.gpu_ids, spectral_norm=opt.spectral_norm )

        if self.isTrain:
            # define loss functions
            kw = {}
            if opt.noisy_labels:
                kw.update({ 'label_flip_prob' : 0.1 })

            if opt.smooth_labels:
                kw.update({ 
                    'target_real_label' : 0.9, 
                    'target_fake_label' : 0.1
                })

            self.criterionGAN = networks.GANLoss(opt.gan_mode, **kw).to(self.device)
            self.criterionL1 = torch.nn.L1Loss()

            if self.opt.inpaint_losses:

                self.criterionTV = TVLoss(normalize=False)

                if self.opt.vgg2:
                    vgg = VGG19_2(slices=[0, 1, 2]).to(self.device)
                    self.criterionPerc = VGGLoss(vgg, 'l1', weights=[1, 0.5, 0.25])
                    self.criterionStyle = None # VGGLoss(vgg, 'gram')
                    self.loss_names.remove('G_style')
                else:
                    vgg = VGG19().to(self.device)
                    self.criterionPerc = VGGLoss(vgg, 'l1')
                    self.criterionStyle = VGGLoss(vgg, 'gram')

            # initialize optimizers; schedulers will be automatically created by function <BaseModel.setup>.

            lrG = opt.lrG if opt.lrG is not None else opt.lr
            lrD = opt.lrD if opt.lrD is not None else opt.lr

            self.optimizer_G = torch.optim.Adam(self.netG.parameters(), lr=lrG, betas=(opt.beta1, 0.999))
            if not self.opt.no_D:
                self.optimizer_D = torch.optim.Adam(self.netD.parameters(), lr=lrD, betas=(opt.beta1, 0.999))

            self.optimizers.append(self.optimizer_G)
            if not self.opt.no_D:
                self.optimizers.append(self.optimizer_D)


    def backward_G(self):

        if self.opt.inpaint_losses:
            self.backward_G_inpainting()
        else:
            self.backward_G_orig()                    

    def backward_G_orig(self):
        """Calculate GAN and L1 loss for the generator"""
        self.loss_G = 0

        # First, G(A) should fake the discriminator
        if not self.opt.no_D:
            fake_AB = torch.cat((self.real_A, self.fake_B), 1)
            pred_fake = self.netD(fake_AB)
            self.loss_G_GAN = self.criterionGAN(pred_fake, True)
            self.loss_G += self.loss_G_GAN

        # Second, G(A) = B
        self.loss_G_L1 = self.criterionL1(self.fake_B, self.real_B) * self.opt.lambda_L1

        self.loss_G += self.loss_G_L1

        # if self.criterionTV is not None:
        #     self.loss_G_tv = self.criterionTV(self.fake_B) * self.opt.lambda_tv
        #     self.loss_G += self.loss_G_tv             
        # combine loss and calculate gradients
        
        self.loss_G.backward()

    def backward_G_inpainting(self):
        """Calculate GAN and L1 loss for the generator"""

        self.loss_G = 0

        if not self.opt.no_D:
            # First, G(A) should fake the discriminator
            fake_AB = torch.cat((self.real_A, self.fake_B), 1)
            pred_fake = self.netD(fake_AB)
            self.loss_G_GAN = self.criterionGAN(pred_fake, True)
            self.loss_G += self.loss_G_GAN
        # Second, G(A) = B

        self.loss_G_L1  = self.criterionL1(self.fake_B, self.real_B) 
        self.loss_G_L1 *= self.opt.lambda_L1

        self.loss_G_tv  = self.criterionTV(self.comp_B, mask=self.tv_mask) 
        self.loss_G_tv *= self.opt.lambda_tv

        self.loss_G_perc  = self.criterionPerc(self.fake_B, self.real_B) + self.criterionPerc(self.comp_B, self.real_B)
        self.loss_G_perc *= self.opt.lambda_perc

        self.loss_G += self.loss_G_L1 + self.loss_G_tv + self.loss_G_perc

        if self.criterionStyle is not None:
            self.loss_G_style  = self.criterionStyle(self.fake_B, self.real_B) + self.criterionStyle(self.comp_B, self.real_B)
            self.loss_G_style *= self.opt.lambda_style
            self.loss_G += self.loss_G_style
        
        # combine loss and calculate gradients
        
        self.loss_G.backward()

    

    def backward_D(self):
        """Calculate GAN loss for the discriminator"""

        # Fake; stop backprop to the generator by detaching fake_B
        fake_AB = torch.cat((self.real_A, self.fake_B), 1)  # we use conditional GANs; we need to feed both input and output to the discriminator
        pred_fake = self.netD(fake_AB.detach())
        self.loss_D_fake = self.criterionGAN(pred_fake, False)
        # Real
        real_AB = torch.cat((self.real_A, self.real_B), 1)
        pred_real = self.netD(real_AB)
        self.loss_D_real = self.criterionGAN(pred_real, True)
        # combine loss and calculate gradients
        self.loss_D = (self.loss_D_fake + self.loss_D_real) * 0.5
        self.loss_D.backward()


    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.

        Parameters:
            input (dict): include the data itself and its metadata information.

        The option 'direction' can be used to swap images in domain A and domain B.
        """
        AtoB = self.opt.direction == 'AtoB'

        self.real_A = input['A' if AtoB else 'B'].to(self.device)
        self.real_B = input['B' if AtoB else 'A'].to(self.device)

        self.mask = input['mask'].to(self.device)

        # if 'patch_edges' in input:
        #    self.patch_edges = input['patch_edges'].to(self.device)
        #else:
        #    warnings.warn('no patch_edges supplied')

        if 'tv_mask' in input:
            self.tv_mask = input['tv_mask'].to(self.device)
            

        if 'style_img' in input:
            self.style_img = input['style_img'].to(self.device)
        else:
            self.style_img = self.real_B


        self.patches = [ m.to(self.device) for m in input['patches']]
        # self.computed_image = 
        # print('patches', len(input['patches']), [p.shape for p in input['patches']])
        self.image_paths = input['A_paths' if AtoB else 'B_paths']



    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        if self.opt.netG in [ 'old', 'c_resnet_6blocks', 'c_resnet_7blocks', 'sc_resnet_6blocks', 'sc_resnet_3blocks', 'cond_unet_256']:
            self.fake_B = self.netG(self.real_A, self.style_img, self.patches)
        else:
            self.fake_B = self.netG(self.real_A)  # G(A)
    
        if self.opt.inpaint_losses:
            self.comp_B = self.fake_B * (1-self.mask) + self.real_B * self.mask

    def optimize_parameters(self):
        self.forward()                   # compute fake images: G(A)
        # update D
        if not self.opt.no_D:
            self.set_requires_grad(self.netD, True)  # enable backprop for D
            self.optimizer_D.zero_grad()     # set D's gradients to zero
            self.backward_D()                # calculate gradients for D
            self.optimizer_D.step()          # update D's weights
        # update G
        if not self.opt.no_D:
            self.set_requires_grad(self.netD, False)  # D requires no gradients when optimizing G
        self.optimizer_G.zero_grad()        # set G's gradients to zero
        self.backward_G()                   # calculate graidents for G
        self.optimizer_G.step()             # udpate G's weights