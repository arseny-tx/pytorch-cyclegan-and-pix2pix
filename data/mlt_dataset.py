import os.path
from data.base_dataset import BaseDataset, get_params, get_transform
from data.image_folder import make_dataset
from PIL import Image

from shared.data import  ICDAR_MLT, mask_from_poly, project_text, inpaint_text, project_seg_map, mk_seg_map

import torch

class MLTDataset(BaseDataset):
    """A dataset class for paired image dataset.

    It assumes that the directory '/path/to/data/train' contains image pairs in the form of {A,B}.
    During test time, you need to prepare a directory '/path/to/data/test'.
    """

    @staticmethod
    def modify_commandline_options(parser, is_train):
        """Add new dataset-specific options, and rewrite default values for existing options.

        Parameters:
            parser          -- original option parser
            is_train (bool) -- whether training phase or test phase. You can use this flag to add training-specific or test-specific options.

        Returns:
            the modified parser.

        By default, the number of channels for input image  is 1 (L) and
        the number of channels for output image is 2 (ab). The direction is from A to B
        """
        parser.set_defaults(input_nc=4, output_nc=3, direction='AtoB')
        return parser
    def __init__(self, opt):
        """Initialize this dataset class.

        Parameters:
            opt (Option class) -- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseDataset.__init__(self, opt)

        self.base_ds = ICDAR_MLT('/nfs/home/anerinovsky/data/tx')
        self.ixs = ICDAR_MLT.get_lang_ixs('English')

        # self.dir_AB = os.path.join(opt.dataroot, opt.phase)  # get the image directory
        assert(self.opt.load_size >= self.opt.crop_size)   # crop_size should be smaller than the size of loaded image
        # self.input_nc = self.opt.output_nc if self.opt.direction == 'BtoA' else self.opt.input_nc
        # self.output_nc = self.opt.input_nc if self.opt.direction == 'BtoA' else self.opt.output_nc

    def __getitem__(self, index):
        """Return a data point and its metadata information.

        Parameters:
            index - - a random integer for data indexing

        Returns a dictionary that contains A, B, A_paths and B_paths
            A (tensor) - - an image in the input domain
            B (tensor) - - its corresponding image in the target domain
            A_paths (str) - - image paths
            B_paths (str) - - image paths (same as A_paths)
        """
        # read a image given a random integer index

        img_path, polys_np, labels, langs = self.base_ds[self.ixs[index]]
        img = Image.open(img_path).convert('RGB')

        seg_map, bg_map, bg_mask, inp_map = mk_seg_map(img, polys_np, labels, langs)

        # apply the same transform to both A and B
        transform_params = get_params(self.opt, img.size)

        bg_transform = get_transform(self.opt, transform_params)
        seg_transform = get_transform(self.opt, transform_params, grayscale=True)

        img_transform = get_transform(self.opt, transform_params)

        img = img_transform(img)
        seg_map = seg_transform(seg_map)
        bg_mask = seg_transform(bg_mask)

        bg_map = bg_transform(bg_map)
        inp_map = bg_transform(inp_map)
        
        A = torch.cat((seg_map, inp_map))
        # print('A:', A.shape)
        # print('B:', img.shape)
        #   print('----')
        return {
            'A': A,  'B': img, 
            'A_paths': str(img_path), 
            'B_paths': str(img_path),
            'mask' : bg_mask
        }

    def __len__(self):
        """Return the total number of images in the dataset."""
        return len(self.ixs )
