import os.path
from data.base_dataset import BaseDataset, get_params, get_transform
from data.image_folder import make_dataset
from PIL import Image

# from shared.data import  ICDAR_MLT, mask_from_poly, project_text, inpaint_text, project_seg_map, mk_seg_map


from shared.mlt_dataset import ICDAR_MLT
from shared.coco_dataset import COCO

from shared.datasets import BoxClustersDataset
from shared.datasets_v2 import BoxClustersDatasetV2, np_to_shapely, shapely_to_np

import torch
        # print('A:', A.shape)
"""Dataset class template

This module provides a template for users to implement custom datasets.
You can specify '--dataset_mode template' to use this dataset.
The class name should be consistent with both the filename and its dataset_mode option.
The filename should be <dataset_mode>_dataset.py
The class name should be <Dataset_mode>Dataset.py
You need to implement the following functions:
    -- <modify_commandline_options>:　Add dataset-specific options and rewrite default values for existing options.
    -- <__init__>: Initialize this dataset class.
    -- <__getitem__>: Return a data point and its metadata information.
    -- <__len__>: Return the number of images.
"""
from data.base_dataset import BaseDataset, get_transform
# from data.image_folder import make_dataset
# from PIL import Image


from torchvision.transforms.functional import to_tensor

from torch.utils.data._utils.collate import default_collate
from collections import defaultdict
import numpy as np


import torchvision.transforms as transforms

def merge_patches(patches, max_n_patches = 8):
    
    p_shape = patches[0].shape
    patches = [ p for p in patches if p.sum() > 0]

    if not patches:
        patches = [torch.zeros(p_shape)]
    elif len(patches) > max_n_patches:
        patches = sorted(patches, key=lambda m: m.sum(), reverse=True)

        stacked_p = torch.stack(patches[max_n_patches:], axis=0).sum(axis=0) > 0

        patches = [*patches[:max_n_patches],  stacked_p.float()] 

    return torch.cat(patches)

def preprocess(src_img, dst_img, bg_mask, masks, patch_edges, opt, style_img=None):

    params = get_params(opt, src_img.size)

    transform = get_transform(opt, params,  convert=False)
    transform_mask = get_transform(opt, params, convert=False)

    bg_mask = to_tensor(transform_mask(bg_mask))
    
    if patch_edges is not None:
        patch_edges = to_tensor(transform_mask(patch_edges))

    masks = [to_tensor(transform_mask(m)) for m in masks]

    norm = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))

    if style_img:
        style_img = norm(to_tensor(transform(style_img)))
    src_img = norm(to_tensor(transform(src_img)))
    dst_img = norm(to_tensor(transform(dst_img)))

    return src_img, dst_img, bg_mask, masks, patch_edges, style_img



def getitem_from_ds(ix, base_ds, opt):
    src_img, dst_img, bg_mask, masks, patch_edges, path, crop = base_ds[ix]
    assert src_img.size == dst_img.size, f"{src_img.size} != {dst_img.size}"

    tv_mask, style_img = None, None
        

    if opt.noise_style_image:
        style_img = base_ds.noise_style_image(dst_img, sigma_coef=opt.style_image_sigma)

    src_img, dst_img, bg_mask, masks, patch_edges, style_img = preprocess(src_img, dst_img, bg_mask, masks, patch_edges, opt, style_img)

    patches = merge_patches(masks, opt.num_patches)

    _, _, poly_py, _ = base_ds.main_boxes[ix]

    x = {
        'A': src_img, 'B': dst_img, 
        'A_paths': str(path), 
        'mask' : bg_mask, # 'patch_edges' : patch_edges,
        'patches' : patches,
        'main_poly' : torch.from_numpy(np.array(poly_py)).float(), 
        'crop' : shapely_to_np(crop), 
        'tv_mask' : patch_edges.bool()
    }

    if style_img is not None:
        x['style_img'] = style_img

    # if tv_mask is not None:
    #        x['tv_mask'] = np.array(tv_mask)[np.newaxis, :, :]

    return x



class TxDataset(BaseDataset):
    """A template dataset class for you to implement custom datasets."""
    @staticmethod
    def modify_commandline_options(parser, is_train):
        """Add new dataset-specific options, and rewrite default values for existing options.

        Parameters:
            parser          -- original option parser
            is_train (bool) -- whether training phase or test phase. You can use this flag to add training-specific or test-specific options.

        Returns:
            the modified parser.
        """
        parser.set_defaults(direction='AtoB')
        parser.add_argument('--num_patches', type=int, default=8, help='number of patches to pass not merged')

        parser.add_argument('--box_cluster_dataset_version', type=int, default=2, help='version of box cluster dataset')
        # parser.add_argument('--box_cluster_data_version', type=int, default=2, help='version of box cluster dataset data')

        # parser.add_argument('--val_dataset', action='store_true', help='version of box cluster dataset data')
        parser.add_argument('--noise_style_image', action='store_true', help='weather add noise to the style image')
        parser.add_argument('--style_image_sigma', type=int, default=6, help='the amount of distortion')

        parser.add_argument('--box_dataset_name', type=str, default='synthtext')
        parser.add_argument('--border_size', type=str, default=80)
        parser.add_argument('--tv_mask', action='store_true')

        return parser

    def __init__(self, opt):
        """Initialize this dataset class.

        Parameters:
            opt (Option class) -- stores all the experiment flags; needs to be a subclass of BaseOptions

        A few things can be done here.
        - save the options (have been done in BaseDataset)
        - get image paths and meta information of the dataset.
        - define the image transformation.
        """
        # save the option and dataset root
        BaseDataset.__init__(self, opt)

        if opt.box_cluster_dataset_version == 2:
            self.base_ds = BoxClustersDatasetV2(name=opt.box_dataset_name, border_size=opt.border_size)
        elif opt.box_cluster_dataset_version == 1:
            self.base_ds = BoxClustersDataset()
        else:
            raise ValueError(f"Wrong box_cluster_dataset_version: {opt.box_cluster_dataset_version}")

        self.opt = opt


    def __getitem__(self, index):

        return getitem_from_ds(index, self.base_ds, self.opt)

       


    def __len__(self):
        """Return the total number of images."""
        return len(self.base_ds)

    @staticmethod
    def collate(batch):
        t_dict = defaultdict(list)

        for b in batch:
            for k in b.keys():
                t_dict[k].append(b[k])

        for k in b.keys():
            if k != 'patches':
                try:
                    t_dict[k] = default_collate(t_dict[k])
                except:
                    print('Failed with ', k)
                    raise
        
        return dict(t_dict)